from typing import List
# import numpy as np
import qrcode

data: List[int] = []

for item in range(20):
    data.append(item)

for elem in data:
    img = qrcode.make(elem)
    img.save(f"qr_{data[elem]}.png")
print(data)
